variable "region" {
  # default     = "us-east-1"
  type        = string
  description = "Region configuration for AWS resources."
}

variable "date" {
  type        = string
  description = "Date of the today execute terraform"
}

variable "credential" {
  type    = string
  default = "credentials"
}