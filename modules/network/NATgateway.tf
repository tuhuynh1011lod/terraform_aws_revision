resource "aws_eip" "terraform-aws-eip-natgtw1" {
  tags = {
    Name = "terraform-aws-eip-natgtw1"
  }
}

resource "aws_eip" "terraform-aws-eip-natgtw2" {
  tags = {
    Name = "terraform-aws-eip-natgtw2"
  }
}


resource "aws_nat_gateway" "terraform-aws-natgw1" {
  allocation_id = aws_eip.terraform-aws-eip-natgtw1.id
  subnet_id     = aws_subnet.terraform-aws-subnet-private1.id
}

resource "aws_nat_gateway" "terraform-aws-natgw2" {
  allocation_id = aws_eip.terraform-aws-eip-natgtw2.id
  subnet_id     = aws_subnet.terraform-aws-subnet-private2.id
}