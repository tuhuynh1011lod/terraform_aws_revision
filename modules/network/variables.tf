variable "region" {
  default     = "us-east-1"
  type        = string
  description = "Region configuration for AWS resources."
}