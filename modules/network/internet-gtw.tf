resource "aws_internet_gateway" "igw-terraform-aws-vpc" {
  vpc_id = aws_vpc.terraform-aws-vpc.id
  tags = {
    Name = "igw-terraform-aws-vpc"
  }
}

