resource "aws_subnet" "terraform-aws-subnet-public1" {
  vpc_id            = aws_vpc.terraform-aws-vpc.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "${var.region}a"
  tags = {
    Name = "terraform-aws-subnet-public1"
  }
}

resource "aws_subnet" "terraform-aws-subnet-private1" {
  vpc_id            = aws_vpc.terraform-aws-vpc.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "${var.region}a"
  tags = {
    Name = "terraform-aws-subnet-private1"
  }
}

resource "aws_subnet" "terraform-aws-subnet-public2" {
  vpc_id            = aws_vpc.terraform-aws-vpc.id
  cidr_block        = "10.0.3.0/24"
  availability_zone = "${var.region}b"
  tags = {
    Name = "terraform-aws-subnet-public2"
  }
}

resource "aws_subnet" "terraform-aws-subnet-private2" {
  vpc_id            = aws_vpc.terraform-aws-vpc.id
  cidr_block        = "10.0.4.0/24"
  availability_zone = "${var.region}b"
  tags = {
    Name = "terraform-aws-subnet-private2"
  }
}

