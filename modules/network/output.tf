output "terraform-aws-vpc" {
  value = aws_vpc.terraform-aws-vpc.id
  description = "ID of vpc."
}

output "terraform-aws-publicsubnet1" {
   value = aws_subnet.terraform-aws-subnet-public1.id
   description = "Public Subnet ID for Availability Zone 1."
}

output "terraform-aws-privatesubnet1" {
    value  = aws_subnet.terraform-aws-subnet-private1.id
    description = "Private Subnet ID for Availability Zone 1."
}

output "terraform-aws-public2" {
  value = aws_subnet.terraform-aws-subnet-public2.id
  description = "Private Subnet ID for Avaialability Zone 2."
}

output "terraform-aws-privatesubnet2" {
  value = aws_subnet.terraform-aws-subnet-private2.id
  description = "Private Subnet ID for Availability Zone 2"
}