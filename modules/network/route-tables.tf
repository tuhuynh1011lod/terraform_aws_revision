resource "aws_route_table" "terraform-aws-rtb-public" {
  vpc_id = aws_vpc.terraform-aws-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw-terraform-aws-vpc.id
  }
  tags = {
    Name = "terraform-aws-rtb-public"
  }
}

resource "aws_route_table" "terraform-aws-rtb-private1" {
  vpc_id = aws_vpc.terraform-aws-vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.terraform-aws-natgw1.id
  }
  tags = {
    Name = "terraform-aws-rtb-private1"
  }
  depends_on = [ aws_nat_gateway.terraform-aws-natgw1 ]
}


resource "aws_route_table" "terraform-aws-rtb-private2" {
  vpc_id = aws_vpc.terraform-aws-vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.terraform-aws-natgw2.id
  }
  tags = {
    Name = "terraform-aws-rtb-private2"
  }
  depends_on = [ aws_nat_gateway.terraform-aws-natgw2 ]
}

# Associate the routing table with the private subnets (one at a time)

resource "aws_route_table_association" "terraform-aws-rtb-associ-public1" {
  subnet_id      = aws_subnet.terraform-aws-subnet-public1.id
  route_table_id = aws_route_table.terraform-aws-rtb-public.id
}

resource "aws_route_table_association" "terraform-aws-rtb-associ-public2" {
  subnet_id      = aws_subnet.terraform-aws-subnet-public2.id
  route_table_id = aws_route_table.terraform-aws-rtb-public.id
}

resource "aws_route_table_association" "terraform-aws-rtb-associ-private1" {
  subnet_id      = aws_subnet.terraform-aws-subnet-private1.id
  route_table_id = aws_route_table.terraform-aws-rtb-private1.id
}

resource "aws_route_table_association" "terraform-aws-rtb-associ-private2" {
  subnet_id      = aws_subnet.terraform-aws-subnet-private2.id
  route_table_id = aws_route_table.terraform-aws-rtb-private2.id
}