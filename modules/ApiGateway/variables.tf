variable "region" {
  default     = "us-east-1"
  type        = string
  description = "Region configuration for AWS resources."
}

# variable "nlb-arn" {
#   type = string
#   description = "ARN of Network Load Balancer"
# }