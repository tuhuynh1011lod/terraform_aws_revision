resource "aws_api_gateway_rest_api" "terraform-aws-api-gateway" {
  name = "terraform-aws-api-gateway"

}

resource "aws_api_gateway_resource" "resource-wsproto" {
  parent_id   = aws_api_gateway_rest_api.terraform-aws-api-gateway.root_resource_id
  path_part   = "wsproto"
  rest_api_id = aws_api_gateway_rest_api.terraform-aws-api-gateway.id
}

resource "aws_api_gateway_method" "resource-wsproto-GET-method" {
  authorization = "NONE"
  http_method   = "GET"
  resource_id   = aws_api_gateway_resource.resource-wsproto.id
  rest_api_id   = aws_api_gateway_rest_api.terraform-aws-api-gateway.id
}

# resource "aws_api_gateway_integration" "resource-wsproto-GET-integration" {
#   http_method = aws_api_gateway_method.resource-wsproto-GET-method.http_method
#   resource_id = aws_api_gateway_resource.resource-wsproto.id
#   rest_api_id = aws_api_gateway_rest_api.terraform-aws-api-gateway.id
#   type        = "HTTP_PROXY"
# }

# resource "aws_api_gateway_deployment" "terraform-aws-api-gateway" {
#   rest_api_id = aws_api_gateway_rest_api.terraform-aws-api-gateway.id

#   triggers = {
#     # NOTE: The configuration below will satisfy ordering considerations,
#     #       but not pick up all future REST API changes. More advanced patterns
#     #       are possible, such as using the filesha1() function against the
#     #       Terraform configuration file(s) or removing the .id references to
#     #       calculate a hash against whole resources. Be aware that using whole
#     #       resources will show a difference after the initial implementation.
#     #       It will stabilize to only change when resources change afterwards.
#     redeployment = sha1(jsonencode([
#       aws_api_gateway_resource.resource-wsproto.id,
#       aws_api_gateway_method.resource-wsproto-GET-method.id
#     #   aws_api_gateway_integration.resource-wsproto-GET-integration.id,
#     ]))
#   }

#   lifecycle {
#     create_before_destroy = true
#   }
# }

# resource "aws_api_gateway_stage" "terraform-aws-api-gateway" {
#   deployment_id = aws_api_gateway_deployment.terraform-aws-api-gateway.id
#   rest_api_id   = aws_api_gateway_rest_api.terraform-aws-api-gateway.id
#   stage_name    = "test"
# }