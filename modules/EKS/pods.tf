resource "aws_eks_fargate_profile" "terraform-aws-cluster-fargate-profile-wsproto" {
  cluster_name           = aws_eks_cluster.terraform-aws-eks.name
  fargate_profile_name   = "terraform-aws-cluster-fargate-profile-wsproto"
  pod_execution_role_arn = aws_iam_role.terraform-aws-fargate-role.arn
  subnet_ids             = [var.private-subnetid-1, var.private-subnetid-2]

  selector {
    namespace = "terraform-aws-cluster-fargate"
  }
}

