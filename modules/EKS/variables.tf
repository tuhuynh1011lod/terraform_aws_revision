variable "region" {
  default     = "us-east-1"
  type        = string
  description = "Region configuration for AWS resources."
}

variable "private-subnetid-1" {
  type        = string
  description = "value"
}

variable "private-subnetid-2" {
  type        = string
  description = "value"
}