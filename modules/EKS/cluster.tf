resource "aws_eks_cluster" "terraform-aws-eks" {
  name     = "terraform-aws-eks"
  role_arn = aws_iam_role.EKS-execution-role.arn

  vpc_config {
    subnet_ids = [var.private-subnetid-1, var.private-subnetid-2]
  }

  # Ensure that IAM Role permissions are created before and deleted after EKS Cluster handling.
  # Otherwise, EKS will not be able to properly delete EKS managed EC2 infrastructure such as Security Groups.
  depends_on = [aws_iam_role_policy_attachment.EKS-execution-role-AmazonEKSClusterPolicy, aws_iam_role_policy_attachment.EKS-execution-role-AmazonEKSVPCResourceController]
}