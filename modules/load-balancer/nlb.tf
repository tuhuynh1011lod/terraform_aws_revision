resource "aws_lb" "nlb" {
  name               = "test-lb-tf"
  internal           = false
  load_balancer_type = "network"
  subnets            = [var.subnet1]
}

resource "aws_lb_listener" "nlb-listener" {
  load_balancer_arn = aws_lb.nlb.arn
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.front_end.arn
  }
}

resource "aws_lb_target_group" "name" {
  
}