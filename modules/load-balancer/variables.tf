variable "vpc_id" {
  type = string
  description = "value of vpc id for which the security group will be created."
}

variable "region" {
  default     = "us-east-1"
  type        = string
  description = "Region configuration for AWS resources."
}

variable "arn-eks-pod-wsproto" {
  type = string
  description = "ARN of eks fargate profile"
}

variable "subnet1" {
  type = string
  description = "subnet that conclude NLB"
}