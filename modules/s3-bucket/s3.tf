resource "aws_s3_bucket" "terraform-aws-s3-bucket" {
  bucket = "terraform-aws-s3-bucket-revision"
}

resource "aws_s3_bucket_ownership_controls" "terraform-aws-ownership" {
  bucket = aws_s3_bucket.terraform-aws-s3-bucket.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_acl" "terraform-aws-s3-bucket-acl" {
  depends_on = [ aws_s3_bucket_ownership_controls.terraform-aws-ownership ]
  bucket = aws_s3_bucket.terraform-aws-s3-bucket.id
  acl = "private"
}

resource "aws_s3_bucket_versioning" "name" {
  bucket = aws_s3_bucket.terraform-aws-s3-bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "name" {
  bucket = aws_s3_bucket.terraform-aws-s3-bucket.id
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "aws:kms"
    }
    bucket_key_enabled = true
  }
}

resource "aws_s3_bucket_website_configuration" "name" {
  bucket = aws_s3_bucket.terraform-aws-s3-bucket.id
  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "error.html"
  }
}