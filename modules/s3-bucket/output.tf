output "s3-bucket" {
  value = aws_s3_bucket.terraform-aws-s3-bucket.arn
}

output "s3-website" {
  value = aws_s3_bucket_website_configuration.name.website_domain
}