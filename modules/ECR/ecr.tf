resource "aws_ecr_repository" "terraform-aws-ecr-repo" {
  name = "terraform-aws-repo"
  image_scanning_configuration {
    scan_on_push = true
  }
  image_tag_mutability = "MUTABLE"
}