module "vpc" {
  source = "./modules/network"
}

module "APIGateway" {
  source = "./modules/ApiGateway"
}

module "EKS" {
  source             = "./modules/EKS"
  private-subnetid-1 = module.vpc.terraform-aws-privatesubnet1
  private-subnetid-2 = module.vpc.terraform-aws-privatesubnet2
}

module "load-balancer" {
  source = "./modules/load-balancer"
  arn-eks-pod-wsproto = module.EKS.arn-eks-pod-wsproto
  vpc_id = module.vpc.terraform-aws-vpc
  subnet1 = module.vpc.terraform-aws-privatesubnet1
}
module "ECR" {
  source = "./modules/ECR"
}

module "EFS" {
  source = "./modules/EFS"
}

module "S3" {
  source = "./modules/s3-bucket"
}